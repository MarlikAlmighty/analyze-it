# github.com/PuerkitoBio/goquery v1.5.0
github.com/PuerkitoBio/goquery
# github.com/andybalholm/cascadia v1.0.0
github.com/andybalholm/cascadia
# github.com/go-redis/redis v6.15.2+incompatible
github.com/go-redis/redis
github.com/go-redis/redis/internal
github.com/go-redis/redis/internal/consistenthash
github.com/go-redis/redis/internal/hashtag
github.com/go-redis/redis/internal/pool
github.com/go-redis/redis/internal/proto
github.com/go-redis/redis/internal/util
# github.com/gorilla/mux v1.7.2
github.com/gorilla/mux
# github.com/technoweenie/multipartstreamer v1.0.1
github.com/technoweenie/multipartstreamer
# golang.org/x/net v0.0.0-20181114220301-adae6a3d119a
golang.org/x/net/html
golang.org/x/net/html/atom
# gopkg.in/telegram-bot-api.v4 v4.6.4
gopkg.in/telegram-bot-api.v4
