package models

type Obj struct {
	Array     Array  `json:"array,omitempty"`
	Timestamp string `json:"timestamp,omitempty"`
}
