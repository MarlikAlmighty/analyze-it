package models

type Post struct {
	Body  string `json:"body,omitempty"`
	Hash  string `json:"hash,omitempty"`
	Image string `json:"image,omitempty"`
	Link  string `json:"link,omitempty"`
	Title string `json:"title,omitempty"`
}
